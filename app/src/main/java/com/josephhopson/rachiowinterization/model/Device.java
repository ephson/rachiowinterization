package com.josephhopson.rachiowinterization.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Joseph Hopson on 6/3/2017.
 */
public class Device extends RealmObject {

    private long createDate;
    @PrimaryKey
    private String id;
    private String status;
    private String timeZone;
    private double latitude;
    private double longitude;
    private String zip;
    private String name;
//    private List scheduleRules;
    private String serialNumber;
    private String macAddress;
    private double elevation;
//    private List webhooks;
    private boolean paused;
    private boolean on;
//    private List flexScheduleRules;
    private String model;
    private String scheduleModeType;
    private boolean deleted;
    private long utcOffset;
    private RealmList<Zone> zones = new RealmList<>();

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public List getScheduleRules() {
//        return scheduleRules;
//    }
//
//    public void setScheduleRules(List scheduleRules) {
//        this.scheduleRules = scheduleRules;
//    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public double getElevation() {
        return elevation;
    }

    public void setElevation(double elevation) {
        this.elevation = elevation;
    }

//    public List getWebhooks() {
//        return webhooks;
//    }
//
//    public void setWebhooks(List webhooks) {
//        this.webhooks = webhooks;
//    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    public boolean isOn() {
        return on;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

//    public List getFlexScheduleRules() {
//        return flexScheduleRules;
//    }
//
//    public void setFlexScheduleRules(List flexScheduleRules) {
//        this.flexScheduleRules = flexScheduleRules;
//    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getScheduleModeType() {
        return scheduleModeType;
    }

    public void setScheduleModeType(String scheduleModeType) {
        this.scheduleModeType = scheduleModeType;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public long getUtcOffset() {
        return utcOffset;
    }

    public void setUtcOffset(long utcOffset) {
        this.utcOffset = utcOffset;
    }

    public RealmList<Zone> getZones() {
        return zones;
    }

    public void setZones(RealmList<Zone> zones) {
        this.zones = zones;
    }
}
