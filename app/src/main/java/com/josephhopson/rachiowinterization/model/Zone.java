package com.josephhopson.rachiowinterization.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Joseph Hopson on 6/3/2017.
 */

public class Zone extends RealmObject {

    @PrimaryKey
    private String id;
    private int zoneNumber;
    private String name;
    private boolean enabled;
//    private Object customNozzle;
//    private Object customSoil;
//    private Object customSlope;
//    private Object customCrop;
//    private Object customShade;
    private double availableWater;
    private int rootZoneDepth;
    private double managementAllowedDepletion;
    private double efficiency;
    private int yardAreaSquareFeet;
    private String imageUrl;
    private int lastWateredDuration;
    private long lastWateredDate;
    private boolean scheduleDataModified;
    private long fixedRuntime;
    private double saturatedDepthOfWater;
    private double depthOfWater;
    private long maxRuntime;
//    private Object wateringAdjustmentRuntimes;
    private long runtimeNoMultiplier;
    private long runtime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getZoneNumber() {
        return zoneNumber;
    }

    public void setZoneNumber(int zoneNumber) {
        this.zoneNumber = zoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

//    public Object getCustomNozzle() {
//        return customNozzle;
//    }
//
//    public void setCustomNozzle(Object customNozzle) {
//        this.customNozzle = customNozzle;
//    }
//
//    public Object getCustomSoil() {
//        return customSoil;
//    }
//
//    public void setCustomSoil(Object customSoil) {
//        this.customSoil = customSoil;
//    }
//
//    public Object getCustomSlope() {
//        return customSlope;
//    }
//
//    public void setCustomSlope(Object customSlope) {
//        this.customSlope = customSlope;
//    }
//
//    public Object getCustomCrop() {
//        return customCrop;
//    }
//
//    public void setCustomCrop(Object customCrop) {
//        this.customCrop = customCrop;
//    }
//
//    public Object getCustomShade() {
//        return customShade;
//    }
//
//    public void setCustomShade(Object customShade) {
//        this.customShade = customShade;
//    }

    public double getAvailableWater() {
        return availableWater;
    }

    public void setAvailableWater(double availableWater) {
        this.availableWater = availableWater;
    }

    public int getRootZoneDepth() {
        return rootZoneDepth;
    }

    public void setRootZoneDepth(int rootZoneDepth) {
        this.rootZoneDepth = rootZoneDepth;
    }

    public double getManagementAllowedDepletion() {
        return managementAllowedDepletion;
    }

    public void setManagementAllowedDepletion(double managementAllowedDepletion) {
        this.managementAllowedDepletion = managementAllowedDepletion;
    }

    public double getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(double efficiency) {
        this.efficiency = efficiency;
    }

    public int getYardAreaSquareFeet() {
        return yardAreaSquareFeet;
    }

    public void setYardAreaSquareFeet(int yardAreaSquareFeet) {
        this.yardAreaSquareFeet = yardAreaSquareFeet;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getLastWateredDuration() {
        return lastWateredDuration;
    }

    public void setLastWateredDuration(int lastWateredDuration) {
        this.lastWateredDuration = lastWateredDuration;
    }

    public long getLastWateredDate() {
        return lastWateredDate;
    }

    public void setLastWateredDate(long lastWateredDate) {
        this.lastWateredDate = lastWateredDate;
    }

    public boolean isScheduleDataModified() {
        return scheduleDataModified;
    }

    public void setScheduleDataModified(boolean scheduleDataModified) {
        this.scheduleDataModified = scheduleDataModified;
    }

    public long getFixedRuntime() {
        return fixedRuntime;
    }

    public void setFixedRuntime(long fixedRuntime) {
        this.fixedRuntime = fixedRuntime;
    }

    public double getSaturatedDepthOfWater() {
        return saturatedDepthOfWater;
    }

    public void setSaturatedDepthOfWater(double saturatedDepthOfWater) {
        this.saturatedDepthOfWater = saturatedDepthOfWater;
    }

    public double getDepthOfWater() {
        return depthOfWater;
    }

    public void setDepthOfWater(double depthOfWater) {
        this.depthOfWater = depthOfWater;
    }

    public long getMaxRuntime() {
        return maxRuntime;
    }

    public void setMaxRuntime(long maxRuntime) {
        this.maxRuntime = maxRuntime;
    }

//    public Object getWateringAdjustmentRuntimes() {
//        return wateringAdjustmentRuntimes;
//    }
//
//    public void setWateringAdjustmentRuntimes(Object wateringAdjustmentRuntimes) {
//        this.wateringAdjustmentRuntimes = wateringAdjustmentRuntimes;
//    }

    public long getRuntimeNoMultiplier() {
        return runtimeNoMultiplier;
    }

    public void setRuntimeNoMultiplier(long runtimeNoMultiplier) {
        this.runtimeNoMultiplier = runtimeNoMultiplier;
    }

    public long getRuntime() {
        return runtime;
    }

    public void setRuntime(long runtime) {
        this.runtime = runtime;
    }
}
