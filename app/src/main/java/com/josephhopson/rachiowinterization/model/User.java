package com.josephhopson.rachiowinterization.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Joseph Hopson on 6/3/2017.
 */
public class User extends RealmObject {

    private long createDate;
    @PrimaryKey
    private String id;
    private String username;
    private String fullName;
    private String email;
    private boolean enabled;
//    private List roles;
//    private List managedDevices;
    private String displayUnit;
    private boolean deleted;
    private RealmList<Device> devices = new RealmList<>();

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

//    public List getRoles() {
//        return roles;
//    }
//
//    public void setRoles(List roles) {
//        this.roles = roles;
//    }
//
//    public List getManagedDevices() {
//        return managedDevices;
//    }
//
//    public void setManagedDevices(List managedDevices) {
//        this.managedDevices = managedDevices;
//    }

    public String getDisplayUnit() {
        return displayUnit;
    }

    public void setDisplayUnit(String displayUnit) {
        this.displayUnit = displayUnit;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public RealmList<Device> getDevices() {
        return devices;
    }

    public void setDevices(RealmList<Device> devices) {
        this.devices = devices;
    }
}
