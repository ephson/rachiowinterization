package com.josephhopson.rachiowinterization.network;

import com.josephhopson.rachiowinterization.model.Device;
import com.josephhopson.rachiowinterization.model.User;
import com.josephhopson.rachiowinterization.model.Zone;
import com.josephhopson.rachiowinterization.network.model.DeviceIdBody;
import com.josephhopson.rachiowinterization.network.model.ZoneIdDurationBody;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Joseph Hopson on 6/5/2017.
 */
public interface RachioAPIService {

    @GET("person/info")
    Call<User> getPersonIdForToken();

    @GET("person/{id}")
    Call<User> getPersonDataForId(@Path("id") String userId);

    // Device

    @GET("device/{id}")
    Call<Device> getDeviceForId(@Path("id") String deviceId);

    @PUT("device/stop_water")
    Call<Void> stopAllWateringForDevice(@Body DeviceIdBody bodyParams);

    @PUT("device/on")
    Call<Void> deviceOn(@Body DeviceIdBody bodyParams);

    @PUT("device/off")
    Call<Void> deviceOff(@Body DeviceIdBody bodyParams);

    // Zone

    @GET("zone/{id}")
    Call<Zone> getZoneForId(@Path("id") String zoneId);

    @PUT("zone/start")
    Call<Void> zoneStart(@Body ZoneIdDurationBody bodyParams);
}
