package com.josephhopson.rachiowinterization.network;

import android.text.TextUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Joseph Hopson on 6/5/2017.
 * Intercepts all API calls and injects the Auth and content type headers
 */
public class ServiceGenerator {

    private static final String BASE_URL = "https://api.rach.io/1/public/";
    private static final String AUTH_TAG = "Bearer";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    // add base url and gson converter
    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass, String accessToken) {

        if (!TextUtils.isEmpty(accessToken)) {

            final String auth = AUTH_TAG+" "+accessToken;

            httpClient.addInterceptor(new Interceptor() {

                @Override
                public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", auth)
                            .header("Content-Type", "application/json")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
        }

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }
}
