package com.josephhopson.rachiowinterization.network.model;

/**
 * Created by Joseph Hopson on 6/6/2017.
 */

public class ZoneIdDurationBody {
    public String id;
    // Duration in seconds (Range is 0 - 10800 (3 Hours) )
    public long duration;
}
