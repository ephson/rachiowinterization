package com.josephhopson.rachiowinterization.network;

import android.app.Activity;

import java.lang.ref.WeakReference;

import retrofit2.Callback;

/**
 * Created by Joseph Hopson on 6/6/2017.
 * Creating a callback with a weak reference so that we do not hold on to and Activity
 * if the lifecycle is trying to kill it.
 */
public abstract class RachioWinterizationCallbackCallback<T> implements Callback<T> {

    private final WeakReference<Activity> activityWeakReference;

    public RachioWinterizationCallbackCallback(Activity activity) {
        activityWeakReference = new WeakReference<>(activity);
    }

    public Activity getActivity() {
        return activityWeakReference.get();
    }
}
