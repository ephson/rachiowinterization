package com.josephhopson.rachiowinterization;

import android.app.Application;

import io.realm.Realm;
import timber.log.Timber;

/**
 * Created by Joseph Hopson on 6/3/2017.
 */

public class RachioWinterizationApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        Timber.plant(new Timber.DebugTree());
    }
}
