package com.josephhopson.rachiowinterization;

/**
 * Created by Joseph Hopson on 6/5/2017.
 */

public class Constants {
    private Constants(){}

    public static final String DEVICE_ID_KEY = "DEVICE_ID_KEY";
    public static final String ZONE_ID_KEY = "ZONE_ID_KEY";
}
