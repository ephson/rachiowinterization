package com.josephhopson.rachiowinterization.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.josephhopson.rachiowinterization.Constants;
import com.josephhopson.rachiowinterization.R;
import com.josephhopson.rachiowinterization.model.Device;
import com.josephhopson.rachiowinterization.model.User;
import com.josephhopson.rachiowinterization.model.UserStateStorage;
import com.josephhopson.rachiowinterization.network.RachioAPIService;
import com.josephhopson.rachiowinterization.network.RachioWinterizationCallbackCallback;
import com.josephhopson.rachiowinterization.network.ServiceGenerator;
import com.josephhopson.rachiowinterization.presentation.adapters.DeviceRecyclerViewAdapter;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class DeviceListActivity extends AppCompatActivity {

    private Realm realm;
    private DeviceRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        realm = Realm.getDefaultInstance();

        View recyclerView = findViewById(R.id.device_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);

        getDevices();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView) {
        adapter = new DeviceRecyclerViewAdapter(realm.where(Device.class).findAll());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        adapter.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = recyclerView.indexOfChild(v);
                Device device = adapter.getItem(pos);
                if(device != null) {
                    Intent intent = new Intent(DeviceListActivity.this, ZoneListActivity.class);
                    intent.putExtra(Constants.DEVICE_ID_KEY, device.getId());
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "null", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void getDevices() {
        if(TextUtils.isEmpty(UserStateStorage.getUserId(this))) {
            Timber.e("User id is empty");
            Toast.makeText(this, "There was an issue.", Toast.LENGTH_SHORT).show();
            UserStateStorage.clear(this);
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
            finish();
        } else {
            // check realm for data 1st
            RealmResults<Device> devices = realm.where(Device.class).findAll();
            if(devices.size() < 1) {
                getDevicesFromService();
            }
        }
    }

    private void getDevicesFromService() {
        RachioAPIService service = ServiceGenerator.createService(RachioAPIService.class,
                UserStateStorage.getAccessToken(this));
        Call<User> userCall = service.getPersonDataForId(UserStateStorage.getUserId(this));
        userCall.enqueue(new RachioWinterizationCallbackCallback<User>(this) {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(getActivity() != null) {
                    User user = response.body();
                    if(user != null) {
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(user);
                        realm.commitTransaction();
                        adapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getApplicationContext(), "Failed got get Device", Toast.LENGTH_SHORT).show();
                        Timber.d("User Object is null or missing the Id onResponse, code:%d", response.code());
                    }
                } else {
                    Timber.d("Activity is null onResponse");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                if(getActivity() != null) {
                    Toast.makeText(getApplicationContext(), "Failed got get Device", Toast.LENGTH_SHORT).show();
                    Timber.d("onFailure", t);
                } else {
                    Timber.d("Activity is null onFailure");
                }
            }
        }); // Async network call
    }
}
