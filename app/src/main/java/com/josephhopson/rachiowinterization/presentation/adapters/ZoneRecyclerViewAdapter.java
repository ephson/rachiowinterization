package com.josephhopson.rachiowinterization.presentation.adapters;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.josephhopson.rachiowinterization.R;
import com.josephhopson.rachiowinterization.model.Zone;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by Joseph Hopson on 6/6/2017.
 * Data -> view adapter for Zones's stored in Realm
 */
public class ZoneRecyclerViewAdapter extends RealmRecyclerViewAdapter<Zone, ZoneRecyclerViewAdapter.ZoneHolder> {

    private View.OnClickListener mClickListener;

    public ZoneRecyclerViewAdapter(@Nullable OrderedRealmCollection<Zone> data) {
        super(data, true);
        setHasStableIds(true);
    }

    @Override
    public ZoneHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.zone_list_content, parent, false);

        ZoneHolder holder = new ZoneHolder(itemView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(ZoneHolder holder, int position) {
        final Zone zone = getItem(position);
        holder.data = zone;
        //noinspection ConstantConditions
        holder.title.setText(zone.getName());
        if(zone.isEnabled()) {
            holder.enabled.setText("Enabled.");
        } else {
            holder.enabled.setText("Disabled.");
        }
    }

    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }

    class ZoneHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView enabled;
        public Zone data;

        ZoneHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.id);
            enabled = (TextView) view.findViewById(R.id.turned_on);
        }
    }
}
