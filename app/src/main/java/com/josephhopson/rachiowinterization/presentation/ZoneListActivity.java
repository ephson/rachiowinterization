package com.josephhopson.rachiowinterization.presentation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.josephhopson.rachiowinterization.Constants;
import com.josephhopson.rachiowinterization.R;
import com.josephhopson.rachiowinterization.model.Device;
import com.josephhopson.rachiowinterization.model.UserStateStorage;
import com.josephhopson.rachiowinterization.model.Zone;
import com.josephhopson.rachiowinterization.network.RachioAPIService;
import com.josephhopson.rachiowinterization.network.RachioWinterizationCallbackCallback;
import com.josephhopson.rachiowinterization.network.ServiceGenerator;
import com.josephhopson.rachiowinterization.presentation.adapters.ZoneRecyclerViewAdapter;

import io.realm.Realm;
import io.realm.Sort;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * An activity representing a list of Devices. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ZoneDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ZoneListActivity extends AppCompatActivity {

    private Realm realm;
    private ZoneRecyclerViewAdapter adapter;
    private boolean mTwoPane;
    private String deviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Intent intent = getIntent();
        deviceId = intent.getStringExtra(Constants.DEVICE_ID_KEY);

        realm = Realm.getDefaultInstance();

        View recyclerView = findViewById(R.id.zone_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);

        if (findViewById(R.id.zone_detail_container) != null) {
            mTwoPane = true;
        }

        if(TextUtils.isEmpty(deviceId)) {
            Timber.e("Device id is empty");
            Toast.makeText(this, "There was an issue.", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getZonesFromService();
    }

    private void setupRecyclerView(@NonNull final RecyclerView recyclerView) {
        adapter = new ZoneRecyclerViewAdapter(realm.where(Device.class).equalTo("id", deviceId).findFirst().getZones().sort("zoneNumber", Sort.ASCENDING));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        adapter.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = recyclerView.indexOfChild(v);
                Zone zone = adapter.getItem(pos);
                if(zone != null) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putString(Constants.ZONE_ID_KEY, zone.getId());
                        ZoneDetailFragment fragment = new ZoneDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.zone_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, ZoneDetailActivity.class);
                        intent.putExtra(Constants.ZONE_ID_KEY, zone.getId());
                        context.startActivity(intent);
                    }
                } else {
                    Timber.e("Failed to get zone from the adapter");
                }
            }
        });
    }

    private void getZonesFromService() {
        RachioAPIService service = ServiceGenerator.createService(RachioAPIService.class,
                UserStateStorage.getAccessToken(this));
        Call<Device> deviceCall = service.getDeviceForId(deviceId);
        deviceCall.enqueue(new RachioWinterizationCallbackCallback<Device>(this) {
            @Override
            public void onResponse(Call<Device> call, Response<Device> response) {
                if(getActivity() != null) {
                    Device device = response.body();
                    if(device != null) {
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(device);
                        realm.commitTransaction();
                        adapter.notifyDataSetChanged();
                    } else {
                        Timber.e("Device Object is null or missing the Id onResponse, code:%d", response.code());
                    }
                } else {
                    Timber.d("Activity is null onResponse");
                }
            }

            @Override
            public void onFailure(Call<Device> call, Throwable t) {
                if(getActivity() != null) {
                    Toast.makeText(ZoneListActivity.this, "Failed to update your Zones.", Toast.LENGTH_SHORT).show();
                    Timber.d("onFailure", t);
                } else {
                    Timber.d("Activity is null onFailure");
                }
            }
        }); // Async network call
    }
}
