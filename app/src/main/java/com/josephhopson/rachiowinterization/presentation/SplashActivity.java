package com.josephhopson.rachiowinterization.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.josephhopson.rachiowinterization.R;
import com.josephhopson.rachiowinterization.model.UserStateStorage;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if(TextUtils.isEmpty(UserStateStorage.getAccessToken(getApplicationContext())) ||
                TextUtils.isEmpty(UserStateStorage.getUserId(getApplicationContext()))) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, DeviceListActivity.class);
            startActivity(intent);
            finish();
        }

    }
}
