package com.josephhopson.rachiowinterization.presentation.adapters;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.josephhopson.rachiowinterization.R;
import com.josephhopson.rachiowinterization.model.Device;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by Joseph Hopson on 6/6/2017.
 * Data -> view adapter for Device's stored in Realm
 */
public class DeviceRecyclerViewAdapter extends RealmRecyclerViewAdapter<Device, DeviceRecyclerViewAdapter.DeviceHolder> {

    private View.OnClickListener mClickListener;

    public DeviceRecyclerViewAdapter(@Nullable OrderedRealmCollection<Device> data) {
        super(data, true);
        setHasStableIds(true);
    }

    @Override
    public DeviceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.device_list_content, parent, false);

        DeviceHolder holder = new DeviceHolder(itemView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(DeviceHolder holder, int position) {
        final Device device = getItem(position);
        holder.data = device;
        //noinspection ConstantConditions
        holder.title.setText(device.getName());
        holder.status.setText(device.getStatus());
        holder.model.setText("Model: "+device.getModel());
        if(device.isOn()) {
            holder.onOff.setText("Device is On.");
        } else {
            holder.onOff.setText("Device is Off.");
        }
        holder.zones.setText(device.getZones().size()+" Zones");
    }

    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }

    class DeviceHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView status;
        TextView onOff;
        TextView model;
        TextView zones;
        public Device data;

        DeviceHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.id);
            status = (TextView) view.findViewById(R.id.status);
            onOff = (TextView) view.findViewById(R.id.turned_on);
            model = (TextView) view.findViewById(R.id.model);
            zones = (TextView) view.findViewById(R.id.zones);
        }
    }
}
