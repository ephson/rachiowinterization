package com.josephhopson.rachiowinterization.presentation;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.josephhopson.rachiowinterization.Constants;
import com.josephhopson.rachiowinterization.R;
import com.josephhopson.rachiowinterization.model.UserStateStorage;
import com.josephhopson.rachiowinterization.model.Zone;
import com.josephhopson.rachiowinterization.network.RachioAPIService;
import com.josephhopson.rachiowinterization.network.RachioWinterizationCallbackCallback;
import com.josephhopson.rachiowinterization.network.ServiceGenerator;
import com.josephhopson.rachiowinterization.network.model.ZoneIdDurationBody;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * A fragment representing a single Zone detail screen.
 * This fragment is either contained in a {@link ZoneListActivity}
 * in two-pane mode (on tablets) or a {@link ZoneDetailActivity}.
 */
public class ZoneDetailFragment extends Fragment {

    private static final long DEFALT_DURATION = 300;

    private String zoneId;
    private Zone zone;
    private Realm realm;

    private TextView enabled_textView;
    private Button toggle_zone_button;

    RachioAPIService service;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ZoneDetailFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        service = ServiceGenerator.createService(RachioAPIService.class,
                UserStateStorage.getAccessToken(getContext()));
        realm = Realm.getDefaultInstance();

        if (getArguments().containsKey(Constants.ZONE_ID_KEY)) {
            zoneId = getArguments().getString(Constants.ZONE_ID_KEY);
            getCachedZone();

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                if(zone != null) {
                    appBarLayout.setTitle(zone.getName());
                } else {
                    appBarLayout.setTitle("Zone");
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.zone_detail, container, false);

        enabled_textView = ((TextView) rootView.findViewById(R.id.enabled_textView));
        toggle_zone_button = ((Button) rootView.findViewById(R.id.toggle_zone_button));
        toggle_zone_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startZone(DEFALT_DURATION); // FIXME make this user configurable
            }
        });

        updateUI();

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateZone();
    }

    private void startZone(long duration) {
        ZoneIdDurationBody params = new ZoneIdDurationBody();
        params.duration = duration;
        params.id = zoneId;
        Call<Void> call = service.zoneStart(params);
        call.enqueue(new RachioWinterizationCallbackCallback<Void>(getActivity()) {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(getActivity() != null) {
                    if(response.code() == 204) {
                        Toast.makeText(getContext(), "Successfully started the zone.", Toast.LENGTH_SHORT).show();
                        updateZone();
                    }
                } else {
                    Timber.d("Activity is null onResponse");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if(getActivity() != null) {
                    Toast.makeText(getContext(), "Failed got start Zone.", Toast.LENGTH_SHORT).show();
                    Timber.d("onFailure", t);
                } else {
                    Timber.d("Activity is null onFailure", t);
                }
            }
        });
    }

    private void getCachedZone() {
        zone = realm.where(Zone.class).equalTo("id", zoneId).findFirst();
    }

    private void updateUI() {
        if (zone != null) {
            if(zone.isEnabled()) {
                enabled_textView.setText("Zone is Enabled");
            } else {
                enabled_textView.setText("Zone is Disabled");
            }
        } else {
            Timber.e("zone is null.");
        }
    }

    private void updateZone() {
        if(!TextUtils.isEmpty(zoneId)) {
            Call<Zone> zoneCall = service.getZoneForId(zoneId);
            zoneCall.enqueue(new RachioWinterizationCallbackCallback<Zone>(getActivity()) {
                @Override
                public void onResponse(Call<Zone> call, Response<Zone> response) {
                    if(getActivity() != null) {
                        Zone newZone = response.body();
                        realm.beginTransaction();
                        zone = realm.copyToRealmOrUpdate(newZone);
                        realm.commitTransaction();
                        updateUI();
                    } else {
                        Timber.d("Activity is null onResponse");
                    }
                }

                @Override
                public void onFailure(Call<Zone> call, Throwable t) {
                    if(getActivity() != null) {
                        Toast.makeText(getContext(), "Failed to get a Zone update.", Toast.LENGTH_SHORT).show();
                        Timber.d("onFailure", t);
                    } else {
                        Timber.d("Activity is null onFailure", t);
                    }
                }
            });
        }
    }
}
