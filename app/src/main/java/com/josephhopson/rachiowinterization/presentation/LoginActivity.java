package com.josephhopson.rachiowinterization.presentation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.josephhopson.rachiowinterization.R;
import com.josephhopson.rachiowinterization.model.UserStateStorage;
import com.josephhopson.rachiowinterization.model.User;
import com.josephhopson.rachiowinterization.network.RachioAPIService;
import com.josephhopson.rachiowinterization.network.ServiceGenerator;
import com.josephhopson.rachiowinterization.network.RachioWinterizationCallbackCallback;

import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

/**
 * On major improvement on the async calls would be to either make them independent of the UI
 * (preferred) or have them be able to detach from a dying activity and reattach to a new one, say
 * for screen rotation. for this test I have just used a weak-reference to the activity so that it
 * does not get held up if the task is running and Android is trying to kill it. and I have made
 * all activities not rebuild on configuration changes (a hack).
 */
public class LoginActivity extends AppCompatActivity {

    /**
     * A dummy authentication store containing known user access tokens.
     * FIXME: in a real app, this would only exist in the QA and or Dev build (using gradle variants).
     */
    private static final String DUMMY_CREDENTIALS = "599c4261-103d-4e9a-b5c4-06558c7fcbe9";

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */

    // UI references.
    private AutoCompleteTextView mAccessTokenView;
    private View mProgressView;
    private View mTextInputLayout;
    private Button mAccessTokenSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set up the login form.
        mAccessTokenView = (AutoCompleteTextView) findViewById(R.id.access_token);
        if(!TextUtils.isEmpty(DUMMY_CREDENTIALS)) {
            mAccessTokenView.setText(DUMMY_CREDENTIALS);
        }

        mAccessTokenSignInButton = (Button) findViewById(R.id.access_token_sign_in_button);
        mAccessTokenSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mTextInputLayout = findViewById(R.id.textInputLayout);
        mProgressView = findViewById(R.id.access_token_progress);
    }

    /**
     * Attempts to access the account specified by the token.
     * If there are form errors (invalid token, missing fields, etc.), the
     * errors are presented and no actual access attempt is made.
     */
    private void attemptLogin() {
        mAccessTokenView.setError(null);
        String accessToken = mAccessTokenView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid accessToken address.
        if (TextUtils.isEmpty(accessToken)) {
            mAccessTokenView.setError(getString(R.string.error_field_required));
            focusView = mAccessTokenView;
            cancel = true;
        } else if (!isTokenValid(accessToken)) {
            mAccessTokenView.setError(getString(R.string.error_invalid_token));
            focusView = mAccessTokenView;
            cancel = true;
        }

        if (cancel) {
            Timber.d("Login data not acceptable, canceling.");
            focusView.requestFocus();
        } else {
            showProgress(true);
            // save for later
            UserStateStorage.saveAccessToken(this, accessToken);

            RachioAPIService service = ServiceGenerator.createService(RachioAPIService.class,
                    accessToken);
            Call<User> userCall = service.getPersonIdForToken();
            userCall.enqueue(new RachioWinterizationCallbackCallback<User>(this) {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if(getActivity() != null) {
                        User user = response.body();
                        showProgress(false);
                        if(user != null && !TextUtils.isEmpty(user.getId())) {
                            UserStateStorage.saveUserId(getApplicationContext(), user.getId());
                            Intent intent = new Intent(LoginActivity.this, DeviceListActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            mAccessTokenView.setError(getString(R.string.error_user_get_failed)); // TODO
                            mAccessTokenView.requestFocus();
                            Timber.d("User Object is null or missing the Id onResponse, code:%d", response.code());
                        }
                    } else {
                        Timber.d("Activity is null onResponse");
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    if(getActivity() != null) {
                        showProgress(false);
                        mAccessTokenView.setError(getString(R.string.error_invalid_token)); // TODO
                        mAccessTokenView.requestFocus();
                    } else {
                        Timber.d("Activity is null onFailure");
                    }
                }
            }); // Async network call
        }
    }

    private boolean isTokenValid(String token) {
        // FIXME verify an api pattern and check it here
        return token.contains("-") && token.length() > 1;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mTextInputLayout.setVisibility(show ? View.GONE : View.VISIBLE);
        mTextInputLayout.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mTextInputLayout.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mAccessTokenSignInButton.setVisibility(show ? View.GONE : View.VISIBLE);
        mAccessTokenSignInButton.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mAccessTokenSignInButton.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
//            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
//            mTextInputLayout.setVisibility(show ? View.GONE : View.VISIBLE);
//            mAccessTokenSignInButton.setVisibility(show ? View.GONE : View.VISIBLE);
    }
}
