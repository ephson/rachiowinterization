# README #

Joseph Hopson

jh@josephhopson.com

This is my partially done solution for the Rachio Winterization app Dev test.

### Missing functionality ###

* The user cant stop a current schedule
* The user can't set a duration for the zone blowout

### UX issues ###

* I quickly hacked this UI together many things could be improved.
* The List items could contain more information, there could be functions embeded in list items.
* Its not very pretty either...
* I cheated on the zone image... getting an image loading library to work with a collapsingtoolbarlayout was going to take too much time.

### Architecture ###

* I used the Default threading solutions for Realm and Retrofit. I would like to have moved to RxJava
* There are no unit tests, there should be.
* Documentation is pretty weak as well.